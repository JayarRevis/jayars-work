from django.contrib import admin
from .models import Question, Choice

class QuestionAdminLook(admin.ModelAdmin):
    list_display = ['pk', 'questions_text', 'pub_date']
admin.site.register(Question, QuestionAdminLook)


class ChoiceAdminLook(admin.ModelAdmin):
    list_display = ['pk', 'question', 'choice_text', 'votes']
admin.site.register(Choice, ChoiceAdminLook)
