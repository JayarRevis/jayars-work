from django.urls import path
from . import views

# app_name = 'polls'
urlpatterns = [
    # Question
    path('', views.index, name='index'),
    path('detail_v1/<int:question_id>', views.detail_v1, name='detail_v1'),
    path('detail_v2/<int:question_id>', views.detail_v2, name='detail_v2'),
    path('question_form', views.questionForm, name='question_form'),
    path('update_question/<int:question_id>',
         views.updateQuestion, name='update_question'),
    path('delete_question/<int:question_id>',
         views.deleteQuestion, name='delete_question'),

    # Choice
    path('choice-index', views.indexChoice, name='choice-index'),
    path('choice-detail/<int:choice_id>',
         views.detailChoice, name='choice-detail'),
    path('choice-create', views.createChoice, name='choice-create'),
    path('choice-update/<int:choice_id>',
         views.updateChoice, name='choice-update'),
    path('choice-delete/<int:choice_id>',
         views.deleteChoice, name='choice-delete'),
]