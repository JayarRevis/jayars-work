from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse
from datetime import datetime
from django.contrib import messages

from .models import Question, Choice
from .forms import QuestionForm, ChoiceForm

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')
    context = {
        'latest_question_list': latest_question_list,
    }

    return render(request, 'polls/question/index.html', context)


def detail_v1(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    context = {
        'question': question,
    }

    return render(request, 'polls/question/detail.html', context)
    

def detail_v2(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/question/detail.html', {'question': question})


def questionForm(request):
    if request.method == 'POST':

        form = QuestionForm(request.POST)

        if form.is_valid():
            new_question = form.save(commit=False)
            new_question.pub_date = datetime.now()
            new_question.save()

            messages.info(
                request, 'you have successfully added the question.')

            return redirect('/polls/question_form')

    else:
        form = QuestionForm()

        context = {
            'form': form,
        }

        return render(request, 'polls/question/addquestion.html', context)


def updateQuestion(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    form = QuestionForm(instance=question)

    if request.method == 'POST':

        form = QuestionForm(request.POST, instance=question)

        if form.is_valid():

            form.save()

            # messages.info(
            #     request, 'you have successfully added the question.')

            return redirect('/polls')

    context = {
        'form': form,
    }

    return render(request, 'polls/question/editdetails.html', context)

def deleteQuestion(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    question.delete()
    return redirect('/polls')


def indexChoice(request):
    choices = Choice.objects.order_by('pk')
    context = {
        'choices': choices,
    }

    return render(request, 'polls/choice/index.html', context)


def detailChoice(request, choice_id):
    choice = get_object_or_404(Choice, pk=choice_id)
    context = {
        'choice': choice,
    }

    return render(request, 'polls/choice/detail.html', context)


def createChoice(request):
    if request.method == 'POST':

        form = ChoiceForm(request.POST)

        if form.is_valid():
            choice = form.save()
            choice.save()

            messages.info(
                request, 'Choice creation is successful.')

        return redirect('/polls/choice-index')

    else:
        form = ChoiceForm()

        context = {
            'form': form,
        }

        return render(request, 'polls/choice/create.html', context)


def updateChoice(request, choice_id):
    choice = get_object_or_404(Choice, pk=choice_id)
    form = ChoiceForm(instance=choice)

    if request.method == 'POST':

        form = ChoiceForm(request.POST, instance=choice)
        if form.is_valid():
            form.save()

            messages.info(
                request, 'Choice update is successful.')

        return redirect('/polls/choice-index')

    context = {
        'form': form,
    }

    return render(request, 'polls/choice/update.html', context)


def deleteChoice(request, choice_id):
    choice = get_object_or_404(Choice, pk=choice_id)
    choice.delete()
    messages.info(
        request, 'Choice deletion is successful.')

    return redirect('/polls/choice-index')
